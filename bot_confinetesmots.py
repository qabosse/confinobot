import tweepy
import configparser, os


# Read config file
config = configparser.ConfigParser()
config.readfp(open('credentials.cfg'))
api_key = config.get('API_CREDENTIALS', 'api_key')
api_secret_key = config.get('API_CREDENTIALS', 'api_secret_key')
access_token = config.get('API_CREDENTIALS', 'access_token')
access_token_secret = config.get('API_CREDENTIALS', 'access_token_secret')


# Authenticate to Twitter
auth = tweepy.OAuthHandler(api_key, api_secret_key)
auth.set_access_token(access_token, access_token_secret)

#Create API
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)


tweets = open('confinetesmots.txt').readlines()

if len(tweets)>0:
    statut=tweets[-1]
    tweets=tweets[:-1]
    open('confinetesmots.txt','w').writelines(tweets)
else:
    statut="Tout a été confiné."

api.update_status(statut)

