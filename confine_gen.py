import pandas
import numpy as np
import random

def le_la_les(genre, nombre, elision):
    determinant = 'les '
    if (nombre == 's'):
        if (genre == 'f'):
            determinant = 'la '
        else:
            determinant = 'le '
        if (elision):
            determinant = "l'"
    return determinant


def ton_ta_tes(genre, nombre, elision):
    determinant = 'tes '
    if (nombre == 's'):
        if (genre == 'f'):
            determinant = 'ta '
        else:
            determinant = 'ton '
        if (elision):
            determinant = 'ton '
    return determinant



def confine(genre, nombre, ortho, f_determinant, elision):
    determinant = f_determinant(genre, nombre, elision)
    return "Confine " + str(determinant) + str(ortho) + '. \n'



# chargement de la bdd
lexical_database = pandas.read_csv('Lexique/Lexique383.tsv', sep='\t')

# sélection du modèle
modele = confine
sortie = 'confine.txt'

# ajout d'une colonne "elision"
# il y a élision dans deux cas :
# Le mot commence par un h et n'est pas dans la liste des h aspirés, c'est un h muet
# le mot commence par une voyelle
liste_h_aspire = pandas.read_csv('h_aspire.txt', sep='\t')
lexical_database['elision'] = (lexical_database['cvcv'].str.get(0) == 'V') | ((lexical_database['ortho'].str.get(0) == 'h' ) & (~ lexical_database['lemme'].isin(liste_h_aspire.h_aspire) ))

# sélection des noms dans la BDD
subset = lexical_database.loc[lexical_database.cgram == 'NOM']

# génération des textes des tweets dans une colonne de la bdd
subset['tweet'] = subset[['genre','nombre','ortho','elision']].apply(lambda x: modele(x.genre, x.nombre, x.ortho, ton_ta_tes, x.elision), axis = 1) 
tweets = subset['tweet'].tolist()

# mélange des textes et sauvegarde
random.shuffle(tweets)
open(sortie, 'w').writelines(tweets)

