# Principe du bot
Ce bot tweete tous les quarts d'heure un message de type *"Confine ton/ta/tes [un nom commun]"*, les noms communs étant prélevés dans la base de données [Lexique3](http://www.lexique.org). Il est très fortement inspiré du [Bot du Cul](https://twitter.com/BotDuCul)
# Structure du programme
Le bot est écrit en Python. Il utilise la librairie [pandas](https://pandas.pydata.org/) pour les opérations sur la base de données et la librairie [tweepy](http://www.tweepy.org/) pour envoyer les tweets.
### Construction du déterminant.
J'ai ajouté une à la BDD une colonne "h aspiré" pour pouvoir déterminer proprement le déterminant de chaque nom (en effet : *habitation* et *harpe* sont féminins, mais on dit ***ton** habitation* et ***ta** harpe* parce que le h de *habitation* est aspiré et celui de *harpe* non). La colonne "h aspiré" est remplie à partir de la liste h\_aspire.txt extraite (manuellement...) de la [page wikipedia](https://fr.wikipedia.org/wiki/H_aspir%C3%A9).
### Construction de la liste des messages
Les contenus des tweets sont tous générés et stockés dans un fichier texte par confine_gen.py , qui est ensuite mélangé pour ne pas tirer les messages dans l'ordre alphabétique.

### Envoi des tweets
Le script bot_confinetesmots.py poste sur Twitter et supprime du fichier texte le message contenu sur la première ligne. Il est exécuté tous les quarts d'heure avec un cron.